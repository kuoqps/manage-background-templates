// JavaScript Document
//关于页面部分的话，会将内容放置到top里面，top会自动维护一个叫做pagelist的对象信息。里面放置当前页面的基础信息

//关于维护的page对象信息

//关闭页面
function cancelPage(pageid){
	if(window.top==window){
		if($Q('[data-menutab][data-id="'+pageid+'"]').hasClass('show')){
            $Q('[data-menutab][data-id="'+pageid+'"]').prev().find('span').click();
		}
		$Q('[data-menutab][data-id="'+pageid+'"]').remove();
		$Q('[data-withact][data-id="'+pageid+'"]').remove();
	}else{
		window.top.cancelPage(pageid);
	}
}

//页面管理的增加页面方法
function createPage(cfgs){
	//cfgs.id:唯一识别编号ID
	//cfgs.label:显示的内容信息
	//cfgs.canClose,是否可以关闭[0,1]
	//cfgs.isShow,//是否显示中	
	//cfgs.url,//显示的链接地址
	//cfgs.isRefresh,//是否刷新
	if(!(cfgs&&cfgs.id)){
		console.log('唯一识别ID为空，不能被创建');
		return ;	
	}
	if(window.top==window){
		//判定为顶层信息
		//判定id是否存在，如果存在直接显示
		if($Q('[data-menutab][data-id="'+cfgs.id+'"]').length){
			//修改流程
			$Q('[data-menutab]').removeClass('show');
			$Q('[data-withact]').removeClass('show');
			$Q('[data-menutab][data-id="'+cfgs.id+'"]').addClass('show');
			$Q('[data-withact][data-id="'+cfgs.id+'"]').addClass('show');
			if(cfgs.isRefresh&&(cfgs.isRefresh==1)){
				var ifname = $Q('iframe[data-id="'+cfgs.id+'"]');
				ifname.length&&ifname.element[0].contentDocument.location.reload();
			}
		}else{
			//创建标签信息
			var lab = $Q('[data-menutab]').first().clone();
			$Q(lab).attr('data-id',cfgs.id);
			$Q(lab).find('span').click(function(){
				//当前label点击发生	
				cfgs.isRefresh = 0;
				createPage(cfgs);
			});
			
			//关闭按钮点击
			$Q(lab).find('i').click(function(){
				//当前label点击发生
				cancelPage(cfgs.id);
			});
			if(cfgs&&cfgs.canClose){
				$Q(lab).find('i').css({'display':'inline-block'});
			}else{
				$Q(lab).find('i').css('display','none');
			}
			
			//配置内容信息
			$Q(lab).find('span').html(((cfgs&&cfgs.label)?cfgs.label:'(null)'));
			$Q('#menutabbox').append(lab);
			//创建页面内容信息
			var cpanel = $Q('#menutabactbox').children().first().clone();
			$Q(cpanel).attr('data-id',cfgs.id);
			$Q(cpanel).find('iframe').attr('src',((cfgs&&cfgs.url)?cfgs.url:''));
			$Q(cpanel).find('iframe').attr('data-id',cfgs.id);
			//判定是否显示当前页面,判定显示当前页面
			if(cfgs&&cfgs.isShow){
				//隐藏已有的label信息
                $Q('[data-menutab]').removeClass('show');
				$Q(lab).addClass('show');
                //隐藏iframe窗口
				$Q('[data-withact]').removeClass('show');
				$Q(cpanel).addClass('show');
				
			}else{
				$Q(cpanel).removeClass('show');	
			}
			
			$Q('#menutabactbox').append(cpanel);
		}
	}else{
		//判定为非顶层信息，直接使用顶层方法进行
		window.top.createPage(cfgs);	
	}
}

//打开一个页面
function openPage(ths){
    //获取当前连接的数据信息
    //判定当前是否为数据节点，怎么判定呢，我也不知道
    var id = $Q(ths).attr('data-id');
    if(!id){
        console.log('请配置data-id信息使用');
        return ;
    }
    //找到上级节点li信息
    var cfgs = {};
    cfgs.id = id;
    cfgs.url = $Q(ths).attr('data-url');
    cfgs.label = $Q(ths).attr('data-title');
    cfgs.isShow = 1;
    cfgs.canClose = 1;
    createPage(cfgs);
}

$Q(document).ready(function(){

    $Q('[data-isdel]').click(function(){
        //需要执行的url地址信息
        var url = $Q(this).attr('data-url');
        var title = $Q(this).attr('data-title');
        var ths = this;
        $Q.confirm({
            tips:title?title:'删除确认？',
            text:'确定删除该数据吗？删除不可恢复',
            sure:function(){
                $Q.loading(true);
                $Q.ajax({
                    url:url,
                    type:'post',
                    dataType:'json',
                    data:{},
                    success:function(rs){
                        if(rs.status==1){
                            $Q.msg('删除成功');
                            $Q(ths).parents('tr').remove();
                        }else{
                            $Q.msg(rs.message);
                        }
                        $Q.loading(false);
                    },
                    error:function(){
                        $Q.loading(false);
                        $Q.msg('删除失败');
                    }
                });
            },
        });
    });

	//基础的加载子页面
	$Q('[data-isloadpage]').click(function(){
		var url = $Q(this).attr('data-url');
		var title = $Q(this).attr('data-title');
		var width = $Q(this).attr('data-width');
		var height = $Q(this).attr('data-height');
		//加载子页面
		$Q.loadpage({
			format:['head','body'],
			url:url,
			tips:title,
			width:width?width:'100%',
            height:height?height:'100%',
            pageBack:function(){
				this.cancel();
			}
		});
	});

    //简单的ajax
    $Q('[data-isajax]').click(function(){
        var url = $Q(this).attr('data-url');
        //加载子页面
        $Q.ajax({
            url:url,
            type:'post',
            dataType:'json',
            success:function(rs){
                if(rs.status==1){
                    window.location.reload();
                }else{
                    $Q.alert(rs.message);
                }
            },
			error:function(){
            	$Q.msg('网络连接超时');
			},
        });
    });
	
	//title自动提示
	$Q('[title]').each(function(){
		var text = $Q(this).attr('title');
		$Q(this).on('dblclick',function(ths){
			var card = $Q.card({
				'format' : ['body'],
				'content' : '<div class="pd-15">'+text+'</div>',
                backgroundClick:function(){
                    card.cancel();
				}
			});
		});
	});
	//折叠菜单的部分功能
	$Q('#systemMenus').find('ul').each(function(e){
		var h = $Q(this).height();
		$Q(this).css('height',h+"px");
		$Q(this).parents('li').addClass('hide-ui');
	});
	//使用打开的方式来
	$Q('#systemMenus').find('a').click(function(){
		if($Q(this).attr('data-url')){
			var id = $Q(this).attr('data-id');
			if(!id){
				console.log('请配置data-id信息使用');
				return ;
			}
			//找到上级节点li信息
			var cfgs = {};
			cfgs.id = id;
			cfgs.url = $Q(this).attr('data-url');
			cfgs.label = $Q(this).attr('data-title');
			cfgs.isShow = 1;
			cfgs.canClose = 1;
			cfgs.isRefresh = 1;
			createPage(cfgs);
		}else{
			if($Q(this).parents('li').hasClass('hide-ui')){
				$Q(this).parents('li').removeClass('hide-ui');	
				$Q(this).parents('li').siblings().addClass('hide-ui');
			}else{
				$Q(this).parents('li').addClass('hide-ui');		
			}
		}
	});
	$Q('#menutabbox').find('span').click(function(){
        var id = $Q(this).parent().attr('data-id');
        if(!id){
            console.log('请配置data-id信息使用');
            return ;
        }
        var cfgs = {};
        cfgs.id = id;
        cfgs.url = $Q(this).attr('data-url');
        cfgs.label = $Q(this).attr('data-title');
        cfgs.isShow = 1;
        cfgs.canClose = 0;
        cfgs.isRefresh = 0;
        createPage(cfgs);
    });
});
var debug;