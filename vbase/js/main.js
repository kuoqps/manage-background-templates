// JavaScript Document
function showPage(a){
	var href = $Q(a).attr('data-url');
	if(href){
		//这个是拥有信息有有效链接
		var cfgs = {};
		cfgs.url = href;
		//尝试获取ID
		var id = $Q(a).attr('data-id');
		if(id){
			cfgs.id = id;
		}else{
			cfgs.id = 'ID' + Math.ceil( Math.random() * Math.pow(10,6));	
		}
		
		//配置标题
		var title = $Q(a).attr('data-title');
		if(title){
			cfgs.title = title;
		}else{
			cfgs.title = href;	
		}
		
		//创建页面的显示内容
		createPage(cfgs);
		
	}else{
		console.warn('请配置[data-url]参数以使用本功能');	
	}
}
//重新加载页面
function reloadPage(){
	if(window.top==window){
		//执行重新加载页面的所有程序信息
		var iframe = $Q('#admin-tabbody').find('iframe.open');
		iframe.first(true).contentWindow.location.reload();
	}else{
		window.top.reloadPage();
	}	
}
//页面管理的增加页面方法
function createPage(cfgs){
	if(window.top==window){
		//判定为顶层信息，这里为启动生成的消息们
		var tab = $Q('#admin-tabhead').find('li[data-id="'+cfgs.id+'"]');
		if(tab.length){
			//这里是有这个卡片的时候
			tab.find('span').click();
		}else{
			//这里是没有卡片的时候
			//增加label
			var title = $Q('<span title="'+cfgs.title+'">'+cfgs.title+'</span>');
			var clobtn = $Q('<i title="点击关闭" class="icon icon-del"></i>');
			var alabel = $Q('<a></a>');
			var label = $Q('<li data-id="'+cfgs.id+'" data-title="'+cfgs.title+'" class="open"></li>');
			$Q(alabel).append(title);
			$Q(alabel).append(clobtn);
			$Q(label).append(alabel);
			$Q('#admin-tabhead').append(label);
			$Q(clobtn).on('click',function(){
				var li = $Q(this).parents('li');
				var id = $Q(li).attr('data-id');
				if($Q(li).prev().length){
					$Q(li).prev().find('span').click();//前一个菜单作为选中
				}else if($Q(li).next().length){
					$Q(li).next().find('span').click();//前一个菜单不存在，则使用后一个菜单作为选中菜单
				}else{
					//最后一个菜单无法被删除
					$Q.alert('最后一个菜单无法被删除');
					return;
				}
				$Q(li).remove();//移除当前元素节点
				$Q('#admin-tabbody').find('iframe[data-id="'+id+'"]').remove();//移除盒子的显示
			});
			$Q(title).on('click',function(){
				var li = $Q(this).parents('li');
				var title = $Q(li).attr('data-title');
				var id = $Q(li).attr('data-id');
				$Q(li).siblings().removeClass('open');
				$Q(li).addClass('open');
				window.top.document.title = title;
				//判断是否有窗口信息
				var iframe = $Q('#admin-tabbody').find('iframe[data-id="'+id+'"]');
				if(iframe.length){
					//窗口存在
					iframe.addClass('open');
				}else{
					//窗口信息不存在，尝试创建信息	
					var iframe = $Q('<iframe data-id="'+id+'" class="open" src="'+cfgs.url+'"></iframe>');
					$Q('#admin-tabbody').append(iframe);
				}
				$Q('#admin-tabbody').find('iframe[data-id="'+id+'"]').siblings().removeClass('open');
			});
			$Q(title).click();
		}
	}else{
		//判定为非顶层信息，直接使用顶层方法进行
		window.top.createPage(cfgs);	
	}
}
//更换主题颜色
function exchangeItemColor(color){
	window.localStorage.setItem("itemColor",color);
	applicationItemColor();
}
//预读主题颜色
function applicationItemColor(){
	var root = window.document.documentElement;
	var color = window.localStorage.getItem("itemColor");
	if(!color){ return;}
	rootStyle = window.getComputedStyle(root);
	root.style.setProperty('--color-index',color);
	if(window.top==window){
		
	}else{
		window.top.applicationItemColor();
	}
}
applicationItemColor();
//查询的相关信息
$Q(document).ready(function(){
	//展示一个页面的相关信息
	$Q('[data-isdel]').click(function(){
        //需要执行的url地址信息
        var url = $Q(this).attr('data-url');
        var title = $Q(this).attr('data-title');
        var ths = this;
        $Q.confirm({
            tips:title?title:'删除确认？',
            text:'确定删除该数据吗？删除不可恢复',
            sure:function(){
                $Q.loading(true);
                $Q.ajax({
                    url:url,
                    type:'post',
                    dataType:'json',
                    data:{},
                    success:function(rs){
                        if(rs.status==1){
                            $Q.msg('删除成功');
                            $Q(ths).parents('tr').remove();
                        }else{
                            $Q.msg(rs.message);
                        }
                        $Q.loading(false);
                    },
                    error:function(){
                        $Q.loading(false);
                        $Q.msg('删除失败');
                    }
                });
            },
        });
    });

	//基础的加载子页面
	$Q('[data-isloadpage]').click(function(){
		var url = $Q(this).attr('data-url');
		var title = $Q(this).attr('data-title');
		var width = $Q(this).attr('data-width');
		var height = $Q(this).attr('data-height');
		//加载子页面
		$Q.loadpage({
			format:['head','body'],
			url:url,
			tips:title,
			width:width?width:'80%',
            height:height?height:'80%',
            pageBack:function(){
				this.cancel();
			}
		});
	});

    //简单的ajax
    $Q('[data-isajax]').click(function(){
        var url = $Q(this).attr('data-url');
        //加载子页面
        $Q.ajax({
            url:url,
            type:'post',
            dataType:'json',
            success:function(rs){
                if(rs.status==1){
                    window.location.reload();
                }else{
                    $Q.alert(rs.message);
                }
            },
			error:function(){
            	$Q.msg('网络连接超时');
			},
        });
    });
	
	//title自动提示
	$Q('[title]').each(function(){
		var text = $Q(this).attr('title');
		$Q(this).on('dblclick',function(ths){
			var card = $Q.card({
				'format' : ['body'],
				'content' : '<div class="pd-15">'+text+'</div>',
                backgroundClick:function(){
                    card.cancel();
				}
			});
		});
	});
});
